from django.shortcuts import render


def redirects404(request):
    return render(request, "index.html")

from django.conf.urls import url
from tastypie.resources import fields, ModelResource
from tastypie.bundle import Bundle
from tastypie.authorization import Authorization
from pollice.models import Poll, Answer


class PollResource(ModelResource):
    answers = fields.ToManyField('pollice.api.AnswerResource', 'answers',
                                 related_name='poll', full=True)

    class Meta:
        queryset = Poll.objects.all()
        resource_name = 'poll'
        list_allowed_methods = ['post']
        detail_allowed_methods = ['get']
        authorization = Authorization()
        always_return_data = True

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}
        if isinstance(bundle_or_obj, Bundle):
            kwargs['slug'] = bundle_or_obj.obj.slug
        else:
            kwargs['slug'] = bundle_or_obj.slug
        return kwargs

    def base_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_list'), name="api_dispatch_list"),
            url(r"^(?P<resource_name>%s)/(?P<slug>[a-zA-Z0-9]{5})/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail")
        ]


class AnswerResource(ModelResource):
    poll = fields.ToOneField(PollResource, 'poll')

    class Meta:
        queryset = Answer.objects.all()
        resource_name = 'answer'
        list_allowed_methods = ['post']
        detail_allowed_methods = ['get', 'put']
        authorization = Authorization()

    def obj_update(self, bundle, **kwargs):
        answer = Answer.objects.get(id=kwargs['pk'])
        answer.votes += 1
        answer.save()
        return answer

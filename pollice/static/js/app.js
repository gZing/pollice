var polliceApp = angular.module('polliceApp', ['ngRoute', 'polliceApp.models']);
    
    polliceApp.run(function($rootScope, $location) {
    $rootScope.location = $location;
    });

    polliceApp.constant('STATIC_URL', '/static');
    polliceApp.constant('API_ROOT', '/api');

    polliceApp.config(['$routeProvider','$httpProvider','$locationProvider', '$interpolateProvider', 'STATIC_URL',
        function($routeProvider, $httpProvider, $locationProvider,
             $interpolateProvider, STATIC_URL){

        $locationProvider.html5Mode(true);
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

        $routeProvider.when('/new', {
                templateUrl : STATIC_URL + '/views/box.html',
                controller : 'poll'
            });

        $routeProvider.when('/', {
                templateUrl : STATIC_URL + '/views/main.html',
                controller : 'main'
            });

        $routeProvider.when('/:slug', {
                templateUrl : STATIC_URL + '/views/display_poll.html',
                controller : 'getController'
            });

        $routeProvider.when('/results/:slug', {
                templateUrl : STATIC_URL + '/views/display_result.html',
                controller : 'resultController'
            });

        $routeProvider.otherwise({ redirectTo: '/' })
           
    }])
    

    polliceApp.controller('poll',['$scope', 'Poll', function($scope, Poll) {
        var j=4;
        $scope.answers=[];
        $scope.create = function(){
            
            var poll = new Poll();
            console.log(poll);
            poll.question = $scope.question;
            poll.answers = []
            var i=1;
            while(true){
                if (!$scope['ans' + String(i)]){
                    break;
                }
                console.log($scope['ans' + String(i)]);
                poll.answers.push({
                    text: $scope['ans' + String(i)]
                });
                i++;
                j++;
            }

            poll.save().then(function(data){
                if (data) {
                    $scope.newPoll = data.slug;
                    console.log($scope.newPoll);
                }

                else{
                    $scope.noPoll = true;
                }
            });
        }

        $scope.newField = function(){
            
            var d1 = document.getElementById("form-field");
            d1.insertAdjacentHTML('beforeend', "<p><input  class='form-control ng-valid ng-pristine' type='text' placeholder='your Options' ng-model='ans"+ j +"'/></p>");
            j++;
        }


    }]);

    polliceApp.controller('getController',['$scope', '$routeParams','$http', 'Poll', 'API_ROOT', function($scope, $routeParams, $http, Poll, API_ROOT) {

        Poll.get($routeParams.slug).then(function(poll){
            $scope.poll = poll;
            console.log($scope.poll);
        });

        $scope.submitVote = function(id){
            console.log("I was clicked!! yay");
            var url = API_ROOT + '/answer/' + id + "/";
            $http.put(url,'{}').then(function(response){
                console.log(response);
                if(response.status < 205){
                    console.log(response);
                    $scope.result = true;
                }

            });

        };


    }]);

    polliceApp.controller('main',['$scope',  function($scope) {

        
    }]);

    polliceApp.controller('resultController',['$scope', '$routeParams', 'Poll', function($scope, $routeParams, Poll) {

        var poll_data;

        Poll.get($routeParams.slug).then(function(poll){
            $scope.poll = poll;
            $scope.complete_values = [];
            $scope.answer_values = [];
            var colors = ['#DB888A',"#0F4D73", "#F3B18C","#FCE4BE", "#F7464A", "#46BFBD","#FDB45C", "#949FB1", "#4D5360",
                          '#9b59b6', '#2ecc71', '#1abc9c', '#f1c40f', '#e67e22', '#95a5a6']
            var col_index = 0;
            for ( i in $scope.poll.answers){
                console.log($scope.poll.answers.length);
                $scope.answer_values.push({ value: $scope.poll.answers[i].votes, color:colors[col_index]});
                $scope.complete_values.push({ text: $scope.poll.answers[i].text, votes: $scope.poll.answers[i].votes, color:colors[col_index]});
                col_index++;

            }
            var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut($scope.answer_values);
            console.log($scope.answer_values);

            var dow = document.getElementById("p1").innerHTML="New text!";
            return true;    

        });

        
    }]);


    

angular.module('polliceApp.models', ['ngRoute'])

.factory('Poll', ['$http', 'API_ROOT',

    function($http, API_ROOT){
        var Poll = function(data){
            angular.extend(this, data);
        };

        Poll.get = function(slug){
            var url = API_ROOT + '/poll/' + slug + "/";
            return $http.get(url).then(function(response){
                return new Poll(response.data);
            });
        };

        Poll.prototype.save = function(){
            var url = API_ROOT + '/poll/' ;
            return $http.post(url, this).then(function(response){
                
                if(response.status < 205){
                    console.log(response);
                    return response.data;
                }
            })
        }

        return Poll;
    }

    ]);

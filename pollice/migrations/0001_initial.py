# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Poll'
        db.create_table(u'pollice_poll', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('question', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal(u'pollice', ['Poll'])

        # Adding model 'Answer'
        db.create_table(u'pollice_answer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(related_name='answers', to=orm['pollice.Poll'])),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('votes', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'pollice', ['Answer'])


    def backwards(self, orm):
        # Deleting model 'Poll'
        db.delete_table(u'pollice_poll')

        # Deleting model 'Answer'
        db.delete_table(u'pollice_answer')


    models = {
        u'pollice.answer': {
            'Meta': {'object_name': 'Answer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'answers'", 'to': u"orm['pollice.Poll']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'votes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'pollice.poll': {
            'Meta': {'object_name': 'Poll'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        }
    }

    complete_apps = ['pollice']
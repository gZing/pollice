from django.contrib import admin
from pollice.models import *


admin.site.register(Poll)
admin.site.register(Answer)

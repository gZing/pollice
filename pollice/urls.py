from django.conf.urls import patterns, include, url, handler404
from django.contrib import admin
from django.views.generic import TemplateView
from pollice.api import AnswerResource, PollResource
from django.conf import settings


admin.autodiscover()
handler404 = 'pollice.views.redirects404'

poll_resource = PollResource()
answer_resource = AnswerResource()
urlpatterns = patterns('',


    # Examples:
    # url(r'^$', 'pollice.views.home', name='home'),
    # url(r'^pollice/', include('pollice.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    (r'^api/', include(answer_resource.urls)),
    (r'^api/', include(poll_resource.urls)),
    (r'^$',TemplateView.as_view(template_name="index.html")),
    
)


if not settings.DEBUG:
    urlpatterns += patterns('', (r'^static/(?P<path>.*)$',
                            'django.views.static.serve',
                            {'document_root': settings.STATIC_ROOT}),
                            )


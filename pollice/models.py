import string
import random

from django.db import models

def id_generator(size=5, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for x in range(size))

class Poll(models.Model):
    slug = models.CharField(max_length=5, default=id_generator, null=False, unique=True)
    question = models.CharField(max_length=150, null=False)

    def __unicode__(self):
        return self.slug


class Answer(models.Model):
    poll = models.ForeignKey(Poll, related_name="answers")
    text = models.CharField(max_length=100, null=False)
    votes = models.IntegerField(default=0)

    def __unicode__(self):
        return self.text[:25]

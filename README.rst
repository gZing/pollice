===========
  Pollice
===========

An app to create and share anonymous online polls.
|Live app: http://pollice.herokuapp.com

Installation
-------------
To run this project follow these steps:

|    $ git clone https://gZing@bitbucket.org/gZing/pollice.git
|    $ cd pollice

Assuming you have Python 2.7, VirtualEnvWrapper installed,
    
|    #. Create your working environment
|    $ mkvirtualenv pollice
|    $ workon pollice
|
|    #. Install dependencies
|    $ pip install -r requirements/local.txt
|
|    #. Run the app
|    $ python manage.py runserver
